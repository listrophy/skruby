# Skruby

**WARNING**: This is nowhere near finished. Do not expect this to do anything useful. It doesn't even load the SceneKit framework yet.

Skruby is an attempt to run SceneKit from within Ruby.

## Installation

Add this line to your application's Gemfile:

    gem 'skruby'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install skruby

## Usage

    rake clean && ruby ext/skruby/extconf.rb && rake compile && ruby test.rb

To quit the application, just type `q`.

## Contributing

1. Fork it ( https://gitlab.com/[my-gitlab-username]/skruby/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
