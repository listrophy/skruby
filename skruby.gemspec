# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'skruby/version'

Gem::Specification.new do |spec|
  spec.name          = "skruby"
  spec.version       = Skruby::VERSION
  spec.authors       = ["Brad Grzesiak"]
  spec.email         = ["brad@bendyworks.com"]
  spec.summary       = %q{Apple's SceneKit, in Ruby}
  spec.description   = %q{Apple's SceneKit, in Ruby}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"

  spec.add_dependency "rake-compiler"

end
