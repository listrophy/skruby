require 'mkmf'

abort 'no termios' unless have_header 'termios.h'

%w(Foundation Cocoa AppKit SceneKit).each do |framework|
  abort "\n#{framework} not found\n\n" unless have_framework framework
end

create_makefile 'skruby/skruby'
