#import "skruby_application.h"
#import "SKRSceneView.h"
#include <termios.h>

@implementation SKApplication

struct termios  termsettings_orig;
struct termios  termsettings_cbreak;

static void restoreTermIOState(void) {
    tcsetattr(0, TCSANOW, &termsettings_orig);
}

static void signalHandler(int sig) {
    restoreTermIOState();
}

- (BOOL)configure {
  // Create a half-screen-sized window
  CGRect mainDisplayBounds = NSRectToCGRect([[NSScreen mainScreen] frame]);
  CGFloat w = mainDisplayBounds.size.width, h = mainDisplayBounds.size.height;
  NSRect bounds = NSMakeRect(w/4.0, h/4.0, w/2.0, h/2.0);
  NSUInteger windowStyle = NSTitledWindowMask | NSClosableWindowMask | NSResizableWindowMask;

  _window = [[NSWindow alloc] initWithContentRect:bounds
                                        styleMask:windowStyle
                                          backing:NSBackingStoreBuffered
                                            defer:NO
                                           screen:[NSScreen mainScreen]];

  NSRect sceneFrame = NSMakeRect(0.0, 0.0, w/2.0, h/2.0);
  _sceneView = [[SKRSceneView alloc] initWithFrame:sceneFrame options:nil];
  [_window setContentView:_sceneView];

  [_window setLevel:NSFloatingWindowLevel];
  [_window makeKeyAndOrderFront:nil];
  return YES;
}

- (BOOL)run {
  // this entire method is copy-pasted from the Apple sample project:
  // avvideowall -> avvideowall/AVVideoWall+TerminalIO.m -> -(BOOL)run;
    __block  BOOL quit = NO;

    NSLog(@"type 'q' to quit");

    dispatch_queue_t keyboardInputQueue = dispatch_queue_create("keyboard input queue", DISPATCH_QUEUE_SERIAL);

    dispatch_async(keyboardInputQueue, ^(void) {
        atexit(restoreTermIOState);
        signal(SIGHUP, signalHandler);
        signal(SIGINT, signalHandler);

        // stash current termios state, switch to cbreak mode
        tcgetattr(0, &termsettings_orig);
        termsettings_cbreak = termsettings_orig;
        termsettings_cbreak.c_lflag &= ~(ICANON | ECHO); // non-canonical mode, disable echo
        termsettings_cbreak.c_cc[VTIME] = 0; // tenths of seconds between bytes
        termsettings_cbreak.c_cc[VMIN] = 1; // num of chars received before returning
        tcsetattr(0, TCSANOW, &termsettings_cbreak);

        while ( !quit ) {
            int curChar = getchar();
            switch (curChar) {
                case 'q':
                case 'Q':
                    quit = YES;
                    break;

                default:
                    break;
            }
        }
    });

    while ( !quit ) {
        NSDate* halfASecondFromNow = [[NSDate alloc] initWithTimeIntervalSinceNow:.5];
        [[NSRunLoop currentRunLoop] runUntilDate:halfASecondFromNow];
        //[halfASecondFromNow release];
        halfASecondFromNow = nil;
    }

    NSLog(@"Quitting");
    dispatch_release(keyboardInputQueue);
    return YES;
}

@end
