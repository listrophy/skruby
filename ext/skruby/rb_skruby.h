#import <Foundation/Foundation.h>
#import "skruby.h"
#import "ruby.h"

void Init_skruby();

VALUE rb_get_sceneName(VALUE);
VALUE rb_set_sceneName(VALUE, VALUE);
void  skruby_mark(SKRuby*);
void  skruby_free(SKRuby*);
VALUE skruby_allocate(VALUE);
VALUE skruby_initialize(VALUE);
