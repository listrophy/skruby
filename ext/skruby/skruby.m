#import "skruby.h"
#include "skruby_application.h"

#import <Foundation/NSObjCRuntime.h>
#import <Cocoa/Cocoa.h>

@interface SKRuby () {
  SKApplication *application;
}
@end

@implementation SKRuby

- (void)debug {
  NSLog(@"Hi from Objective-C!");

  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  // In a command line applicaton, NSApplicationLoad is required to get an NSWindow to become key and forefront.
  (void)NSApplicationLoad();

  application = [[SKApplication alloc] init];
  [application configure];
  [application run];
  //[application release];

  //[pool release];
}

@end
