#import "rb_skruby.h"

// Defining a space for information and references about the module to be stored internally
VALUE     rb_cSKRuby  = Qnil;
SKRuby *skruby          = nil;

VALUE rb_debug(VALUE self) {
  [skruby debug];
  return Qnil;
}

void skruby_mark(SKRuby *skruby) {
  //rb_gc_mark([skruby sceneName]);
}

void skruby_free(SKRuby *skruby) {
  [skruby release];
}

VALUE skruby_allocate(VALUE klass) {
  if(!skruby) {
    skruby = [[SKRuby alloc] init];
  }

  return Data_Wrap_Struct(klass, skruby_mark, skruby_free, skruby);
}

VALUE skruby_initialize(VALUE self) {
// Can't use Data_Get_Struct, as it performs strict type checking for a C struct... and rejects ObjC Objects.
//  Data_Get_Struct(self, SKRuby, skruby);
  skruby = (SKRuby*)DATA_PTR(self);
  return self;
}

void Init_skruby() {
  skruby      = [[SKRuby alloc] init];
  rb_cSKRuby  = rb_define_class("SKRuby", rb_cObject);

  rb_define_alloc_func(rb_cSKRuby, skruby_allocate);

  rb_define_method(rb_cSKRuby, "initialize", skruby_initialize,  0);
  rb_define_method(rb_cSKRuby, "debug",      rb_debug,   0);
}
