#import "SKRSceneView.h"

@implementation SKRSceneView

- (id)initWithFrame:(NSRect)frame options:(NSDictionary *)options {
  self = [super initWithFrame:frame options:options];
  self.scene = [SCNScene scene];
  self.backgroundColor = [NSColor blueColor];

  SCNNode *obj = [SCNNode node];
  obj.geometry = [SCNSphere sphereWithRadius:2.0];
  SCNMaterial *mat = [SCNMaterial material];
  mat.ambient.contents = [NSColor greenColor];
  obj.geometry.firstMaterial = mat;

  [self.scene.rootNode addChildNode:obj];
  return self;
}


@end
