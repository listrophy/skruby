#pragma once
#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@class SKRSceneView;

@interface SKApplication : NSObject {
  NSWindow *_window;
  SKRSceneView *_sceneView;
}
- (BOOL)configure;
- (BOOL)run;
@end
